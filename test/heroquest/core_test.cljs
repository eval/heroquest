(ns heroquest.core-test
  (:require [cljs.test :refer-macros [async deftest is testing are use-fixtures]]
            [heroquest.core :as hq]))

(def board-3x2
  [{:rows 2 :cols 3}
   [{} {} {}
    {} {} {}]])

(deftest neighbour-ixs
  (are [board ix expected] (= (sort (hq/neighbour-ixs ix board)) expected)
    board-3x2 0 '(1 3)
    board-3x2 1 '(0 2 4)
    board-3x2 4 '(1 3 5)
    board-3x2 5 '(2 4)))


(deftest simple-reveal-buttons
  "All unseen neighbours of seen cells"
  (are [cells expected] (= expected (sort (hq/reveal-button-ixs [{:cols 2 :rows 2} cells])))
    [{} {:seen true}
     {} {}]           '(0 3)
    [{} {:seen true}
     {} {:seen true}] '(0 2)
    [{:seen true} {}
     {} {}]           '(1 2)
    [{} {}
     {} {}]           '()
))

(deftest blocked-reveal-buttons
  "Walls reduce reveals"
  (are [cells expected] (= expected (sort (hq/reveal-button-ixs [{:cols 2 :rows 2} cells])))
    [{} {:seen true :walls [:bottom]}
     {} {}]           '(0)
    [{} {:seen true}
     {} {:seen true}] '(0 2)
    [{:seen true} {}
     {} {}]           '(1 2)
    [{} {}
     {} {}]           '()))

(deftest scoped-reveal-buttons
  "Passing a collection of cells will scope the reveal-buttons to neighbours of these cells."
  (are [cells cell expected] (= expected (hq/reveal-button-ixs [{:cols 2 :rows 2} cells] :cell cell))
    [{:seen true} {}
     {:seen true} {}] 0 [1]
    [{:seen true :walls [:right]} {}
     {:seen true} {}] 0 []
))

;; following not really needed as a door can be added to cell on both sides.
#_(deftest non-blocked-reveals
  "Door on either side of wall "
  (are [cells expected] (= expected (sort (hq/reveal-button-ixs [{:cols 2 :rows 1} cells])))
    [{:doors [:right] :walls [:right]} {:seen true :walls [:left]}]  '(0)
))

(deftest neighbours-with-blocking
  (are [cells blocking ix expected] (= (hq/neighbours-with-blocking ix [{:cols 2 :rows 2} cells] blocking) expected)
    [{:doors [:right]} {:doors [:left]} {} {}] [] 0 {:right [1 0] :bottom [0 1]} ;; no blocking
    [{:doors [:right]} {:doors [:left]} {} {}] [:doors] 0 {:bottom [0 1]}        ;; doors blocking
    [{:walls [:right]} {:walls [:left]} {} {}] [:walls] 0 {:bottom [0 1]}        ;; walls blocking
    [{:rock true} {} {} {}] [:rock] 0 {}                                         ;; rock is blocking
))

(deftest reveal-cross
  "All cells in the same row and column are revealed."
  (are [cells ix expected] (= (hq/reveal-cross ix [{:rows 2 :cols 2} cells]) expected)
    [{} {} {} {}] 0 {:bottom [2] :right [1] :center [0] :left [] :top []}
    [{} {} {} {}] 1 {:bottom [3] :left [0] :center [1] :top [] :right []}
))

(deftest reveal-ixs
  (are [cells cross expected] (= (sort (hq/reveal-ixs cross [{:cols 2 :rows 3} cells])) expected)
    [{} {}
     {} {}
     {} {}] {:top [0] :right [3] :bottom [4] :center [2]} [0 1 2 3 4 5]
    [{} {}
     {} {}
     {} {}] {:top [1] :left [2] :bottom [5] :center [3]} [0 1 2 3 4 5]

)
)

(deftest reachable-neighbours-ixs
  (are [cells ix expected] (= expected (sort (hq/reachable-neighbours-ixs ix [{:cols 2 :rows 2} cells])))
    ;; simple
    [{} {}
     {} {}] 1 '(0 3)

    ;; simple II
    [{} {}
     {} {}] 0 '(1 2)

    ;; rocks have no neighbours
    [{} {:rock true}
     {} {}] 1 '()

    ;; walls block
    [{} {:walls [:bottom]}
     {} {}] 1 '(0)

    ;; doors make cells reachable (wall no longer blocks)
    [{} {:seen true :walls [:bottom] :doors [:bottom]}
     {} {}] 1 '(0 3)
))

(deftest assoc-in-reveal
  (are [reveals cells expected] (= expected (second (hq/assoc-in-reveal reveals [{:rows 1 :cols 2} cells])))
    '(0) [{} {}]
        [{:reveal true} {}]
    '(0 1) [{} {}]
        [{:reveal true} {:reveal true}]
))

(deftest reset-reveals!
  (is (= [{} {}] (last (hq/reset-reveals! [{:rows 1 :cols 2} [{:reveal true} {}]])))))

(deftest ix->coord
  (are [ix expected] (= (hq/ix->coord ix {:cols 3}) expected)
    0 [0 0]
    3 [0 1]
    4 [1 1]
    5 [2 1]
    6 [0 2]
))

(deftest coord->ix
  (are [coord expected] (= (hq/coord->ix coord {:cols 3}) expected)
    [0 0] 0
    [0 1] 3
    [1 1] 4
    [2 1] 5
    [0 2] 6
))

(deftest valid-coord?
  (are [coord dims expected] (= (true? (hq/valid-coord? coord dims)) expected)
    [0 0] {:cols 1 :rows 1} true
    [1 0] {:cols 1 :rows 1} false
    [1 1] {:cols 2 :rows 2} true
    [3 3] {:cols 2 :rows 2} false
))

(deftest indexed-cells
  (are [cells expected] (= (hq/indexed-cells [{} cells]) expected)
    [{} {}] '([0 {}] [1 {}])

))

(deftest filtered-cells
  (are [cells f expected] (= (hq/filtered-cells [{} cells] f) expected)
    [{} {}] identity '(0 1)
    [{:seen true} {}] :seen '(0)
    [{} {}] :see '()
))

(deftest reveal
  (are [cells ix expected] (= (hq/reveal [ix] [{:cols 2 :rows 2} cells]) [{:cols 2 :rows 2} expected])
    [{} {}] 0 [{:seen true} {}]
    [{} {} {}] 2 [{} {} {:seen true}]
))
