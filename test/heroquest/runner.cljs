(ns heroquest.runner
  (:require [cljs.test :as test]
            [doo.runner :refer-macros [doo-all-tests doo-tests]]
            [heroquest.core-test]
          ))

(doo-tests 'heroquest.core-test)
