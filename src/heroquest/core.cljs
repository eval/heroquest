(ns heroquest.core
  (:require [reagent.core :as reagent :refer [atom]]
            [goog.math :as math]            
            [clojure.string :as str]
            [heroquest.util :as util]
            [com.rpl.specter :as s]))

(enable-console-print!)

(def empty-board [{:cols 3 :rows 3}
                  [{:walls [:bottom]} {} {}
                   {} {} {}
                   {} {} {}]])

(def cell-size 50)

(defn coord->position [[x y]]
  {:left (* cell-size x) :top (* cell-size y)})

(defn ix->coord
  "Translates index in a board to a coordinate"
  [ix {cols :cols}]
  [(math/modulo ix cols) (math/safeFloor (/ ix cols))])

(defn coord->ix [[x y] {cols :cols}]
  (+ x (* y cols)))

(defn valid-coord? [coord {:keys [cols rows]}]
  (let [valid-coords (for [x (range cols) y (range rows)] [x y])]
    (util/in? valid-coords coord)))

(defn neighbours [ix [dims cells]]
  "Yields neighbours of cell, e.g. {:top [1 2] :right [2 3]}.
Coordinates are always valid coordinates (ie on the board)."
  (let [abs-coord (partial mapv + (ix->coord ix dims)) ;; relative coord -> real coord
        invalid-coord? (fn [c] (not (valid-coord? c dims)))]
    (->> {:top [0 -1] :right [1 0] :bottom [0 1] :left [-1 0]}
         (s/transform [s/MAP-VALS] abs-coord)
         (s/setval [s/MAP-VALS invalid-coord?] s/NONE))))

(defn neighbours-with-blocking [ix [dims cells :as board] blocking]
  "Neighbours but with blocking taking into account.
blocking can be a vector like [:doors :walls]."
  (let [cell (nth cells ix)
        nbs (neighbours ix board)
        rock-blocking? (util/in? blocking :rock)
        doors-walls-blocking (filter #{:doors :walls} blocking)
        blocking-directions (reduce #(apply conj %1 (or (%2 cell) [])) [] doors-walls-blocking)
        non-blocking-directions (remove #(util/in? blocking-directions %) [:top :right :bottom :left])]
    (cond
      (and rock-blocking? (:rock cell)) {}
      :else (select-keys nbs non-blocking-directions))))

(defn indexed-cells [[_ cells]]
  (map-indexed vector cells))

(defn filtered-cells
  ([board] (filtered-cells board identity))
  ([board f]
   (reduce (fn [acc [ix cell]]
             (if (f cell) (conj acc ix) acc)) [] (indexed-cells board))))

(defn neighbour-ixs [ix [dims _ :as board]]
  (map #(coord->ix % dims) (vals (neighbours ix board))))

(defn reachable-neighbours-ixs [ix [dims cells :as board]]
  "Neighbours not separated by walls or rocks"
  (let [nb (neighbours ix board)
        cell (nth cells ix)
        ;; walls with door are not blocking
        blocking-walls (remove #(util/in? (:doors cell) %) (:walls cell))]
    (cond
      (:rock cell) []
      :else (map #(coord->ix % dims) (vals (into {} (s/select [s/ALL #(util/not-in? blocking-walls (key %))] nb)))))))

(defn reveal-button-ixs
  ([[dims cells :as board]]
   "Cells that need a reveal-button"
   (let [seen-ixs (filtered-cells board #(:seen %))
         seen? #(util/in? seen-ixs %)
         neighbour-ixs (reduce (fn [acc ix] (into acc (reachable-neighbours-ixs ix board))) [] seen-ixs)]
     (remove seen? neighbour-ixs)))
  ([board & {cell :cell}]
   "Reveal-buttons only needed for a specific cell."
   (let [all (reveal-button-ixs board)
         cell-nbs (reachable-neighbours-ixs cell board)]
     (remove #(util/not-in? cell-nbs %) all))))

(defn- assoc-in-cells-old [[dims cells] ixs f]
  "Assoc into specific cells"
  (->> cells
      (mapv #(assoc %2 :ix %1) (iterate inc 0))
      (s/transform [s/ALL (s/selected? [:ix (set ixs)])] f)
      (s/transform [s/ALL #(:ix %)] #(dissoc % :ix))
      (conj [dims])))

(defn assoc-in-cells [board ixs key value]
  (assoc-in-cells-old board ixs #(assoc % key value)))

(defn dissoc-in-cells [board ixs key]
  (assoc-in-cells-old board ixs #(dissoc % key)))

(defn assoc-in-reveal [ixs board]
  (assoc-in-cells board ixs :reveal true))

(defn reset-reveals! [[_ cells :as board]]
  "Removes all reveals"
  (dissoc-in-cells board (range (count cells)) :reveal))

(defn reveal [ixs board]
  "Marks the ixs cells as seen."
  (-> board
      (assoc-in-cells ixs :seen true)))

(defn reveal-cross
  "Full row and column of cells that are revealed when clicking on cell ix. E.g. {:top [0] :left [] :right [3] :bottom []}"
  ([ix board]
   (let [nbs (neighbours-with-blocking ix board [:doors :walls :rock])
         acc (zipmap [:top :right :bottom :left] (repeat []))]
     (reveal-cross board nbs (assoc acc :center [ix]))))
  ([[dims _ :as board] [[direction coord] & nbs] acc]
   (if (nil? direction)
     acc
     (let [ix (coord->ix coord dims)
           buur (direction (neighbours-with-blocking ix board [:doors :walls :rock]))
           buren (into {} nbs)
           new-acc (update-in acc [direction] conj ix)]
       (recur board (if buur (assoc buren direction buur) buren) new-acc)))))

(defn reveal-y
  [ix board & {:keys [dir max-depth]}]
  "Given a cell, selects upto max-depth cells in direction dir that can be revealed."
  (take max-depth (dir (reveal-cross ix board))))

(defn calc-max-depth [ixs [_ cells]]
  (let [rock-last? (if-let [latest (last ixs)] (:rock (nth cells latest)))]
    (if rock-last? (dec (count ixs)) (count ixs))))

(defn reveal-edge [ixs board & {:keys [dir max-depth]}]
  (first (reduce
    (fn [[result depth] ix]
      (let [moar (reveal-y ix board :dir dir :max-depth depth)]
        [(apply conj result moar) (calc-max-depth moar board)]))
    [[] max-depth]
    ixs)))

(defn reveal-ixs [cross board]
    (let [result (into [] (flatten (vals cross)))
          {:keys [right left bottom top]} cross
          [bottom-depth top-depth] (map #(calc-max-depth % board) [bottom top])]
      (reduce
       (fn [acc [ixs dir depth]]
         (apply conj acc (reveal-edge ixs board :dir dir :max-depth depth)))
       result
       [[right :bottom bottom-depth] [right :top top-depth]
        [left :bottom bottom-depth] [left :top top-depth]])))

;; --- events

(defn reveal-clicked [board-atom coord]
  (let [[dims _ :as board] @board-atom
        ix (coord->ix coord dims)
        rixs (reveal-ixs (reveal-cross ix board) board)]
    (reset! board-atom (reveal rixs board))))

;; --- components

(comment
  #_(s/transform [s/LAST (s/srange 0 2) s/FIRST] #(assoc % :seen true) [{:rows 3 :cols 4} [{} {}]])

  #_(s/transform [s/ALL #{1}] inc [1 2])

  #_(s/transform [s/ALL #{1}] prn (map-indexed vector [{} {}]))

  #_(s/transform [s/ALL (path (partial #{1 2}))] prn [{:ix 0} {:ix 2}])

  #_(s/transform [s/ALL] #(when (#{0} (ffirst %)) (assoc % :seen true)) [{:ix 0} {:ix 2}]))

(defn search-button [coord]
  (let [style {:padding 6 :margin 8}
        style (merge style {:position "absolute"} (coord->position coord))]
    [:a.action.link.dim.gray.dib.h2.w2.br-100.mr3.pa2.bg-near-white.ba.b--black-10 {:style style :href "#" :title "search"}
     [:svg {:view-box "0 0 1792 1792" :style {:fill "currentcolor"}}
      [:title "Search"]
      [:path {:d "M1216 832q0-185-131.5-316.5t-316.5-131.5-316.5 131.5-131.5 316.5 131.5 316.5 316.5 131.5 316.5-131.5 131.5-316.5zm512 832q0 52-38 90t-90 38q-54 0-90-38l-343-342q-179 124-399 124-143 0-273.5-55.5t-225-150-150-225-55.5-273.5 55.5-273.5 150-225 225-150 273.5-55.5 273.5 55.5 225 150 150 225 55.5 273.5q0 220-124 399l343 343q37 37 37 90z"}]]]))


(defn reveal-button [coord & {:keys [action] :or {action #(js/alert "Revealing!")}}]
  (let [style {:padding 8 :margin 8}
        style (merge style {:position "absolute"} (coord->position coord))]
    [:a.action.link.dim.gray.dib.h2.w2.br-100.mr3.pa2.bg-near-white.ba.b--black-10 {:style style :on-click #(action coord) :title "reveal"}
     [:svg {:view-box "0 0 1792 1792" :style {:fill "currentcolor"}}
      [:title "Reveal"]
      [:path {:d "M1664 960q-152-236-381-353 61 104 61 225 0 185-131.5 316.5t-316.5 131.5-316.5-131.5-131.5-316.5q0-121 61-225-229 117-381 353 133 205 333.5 326.5t434.5 121.5 434.5-121.5 333.5-326.5zm-720-384q0-20-14-34t-34-14q-125 0-214.5 89.5t-89.5 214.5q0 20 14 34t34 14 34-14 14-34q0-86 61-147t147-61q20 0 34-14t14-34zm848 384q0 34-20 69-140 230-376.5 368.5t-499.5 138.5-499.5-139-376.5-368q-20-35-20-69t20-69q140-229 376.5-368t499.5-139 499.5 139 376.5 368q20 35 20 69z"}]]]))


(def special-pieces {:stairs {:width 100}})

(defonce app-state (atom {:text "Hello world!"}))

(defn door-component [coord dir]
  (let [src (if (some #{dir} [:top :bottom]) "images/door-h.png" "images/door-v.png")]
    [:img {:class (str "door-" (name dir)) :src src :style (merge {:position "absolute"} (coord->position coord))}]))

(defn rock-component [coord]
  [:img {:src "images/rocks.png" :style (merge {:position "absolute" :width cell-size} (coord->position coord))}])

(defn monster-component [coord monster]
  (let [abs-position (coord->position coord)
        apply-correction #(merge-with + abs-position %)
        style (condp = monster
                :gargoyle (apply-correction {:width 65 :left -13 :top -3})
                abs-position)]
    [:img.monster {:src (str "images/" (name monster) ".png") :style (merge {:position "absolute"} style)}])
)

(merge-with + {:top 10 :bottom 10} {:bottom -5})

(defn doors-by-ix [[dims cells] & {:keys [only] :or {only cells}}]
  "Yields collection of door-components, e.g. {0 [:top] 3 [:right :left]}.
Pass subset to consider to :only."
  (reduce (fn [acc [ix cell]]
            (if-let [doors (:doors cell)]
              (if (util/in? only ix) (assoc acc ix doors) acc)
              acc))
          {}
          (map-indexed vector cells)))

(defn door-components [[dims cells :as board]]
  (for [[ix door-positions] (doors-by-ix board :only (filtered-cells board :seen))]
    (for [pos door-positions]
      ^{:key ["door" ix]}  [door-component (ix->coord ix dims) pos])))

(defn rock-components [[dims cells :as board]]
  (for [ix (filtered-cells board #(and (:rock %) (:seen %)))]
    ^{:key (str "rock-" ix)} [rock-component (ix->coord ix dims)]))

(defn monster-components [[dims cells :as board]]
  (for [ix (filtered-cells board #(and (:monster %) (:seen %)))]
    ^{:key (str "monster-" ix)} [monster-component (ix->coord ix dims) (:monster (nth cells ix))]))

(defn cell-component [{:keys [last-row? last-col? coord walls seen? ix]}]
  (let [wall-classes (map #(str "wall-" (name %1)) walls)
        class-map {"last-row" last-row? "last-col" last-col? "seen" seen?}
        classes (keys (filter (partial second) class-map))
        all-classes (apply conj classes wall-classes)]
    [:div.cell {:style (coord->position coord) :class (str/join " " all-classes)} (str coord)]))

(defn cell-components [[{:keys [rows cols] :as dims} cells :as board]]
  (for [[ix cell] (map-indexed vector cells)]
    (let [coord (ix->coord ix dims)
          last-row? (= (second coord) (dec rows))
          last-col? (= (first coord) (dec cols))]
      ^{:key coord} [cell-component {:walls (:walls cell) :seen? (:seen cell) :last-row? last-row? :last-col? last-col? :coord coord :ix ix}])))

(defn action-buttons [[dims cells :as board] action]
  (let [reveals (reduce (fn [acc [ix cell]] (if (:reveal cell) (conj acc ix) acc)) [] (map-indexed vector cells))]
    (for [ix reveals]
      ^{:key (str "action-" ix)} [reveal-button (ix->coord ix dims) :action action])))

(defn board-component [board-atom]
  (let [[{:keys [rows cols] :as dims} cells :as board] @board-atom
        double-border-size 6
        player-ixs (filtered-cells board :player)
        reveal-buttons (flatten (map #(reveal-button-ixs board :cell %) player-ixs))
        board (->> board reset-reveals! (assoc-in-reveal reveal-buttons))
        board-height (+ double-border-size (* cell-size rows))
        board-width (+ double-border-size (* cell-size cols))
        reveal-action (partial reveal-clicked board-atom)]
    [:div.board {:style {:height board-height :width board-width}}
     (cell-components board)
     (door-components board)
     (rock-components board)
     (monster-components board)
     (action-buttons board reveal-action)]))

(if-let [node (.getElementById js/document "app")]
  (reagent/render-component [board-component empty-board] node))

(defn on-js-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
