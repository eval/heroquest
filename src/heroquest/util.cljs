(ns heroquest.util)

(defn in? 
  "true if coll contains elm"
  [coll elm]  
  (some #(= elm %) coll))

(def not-in? (complement in?))
