(ns heroquest.devcards.core
  (:require [heroquest.core :as hq]
            [reagent.core :as reagent]
            [devcards.core :as dc :refer [defcard-rg] :refer-macros [defcard]]
            [goog.math :as math]))

(enable-console-print!)
(dc/start-devcard-ui!)

(defcard styled-board
  "Showing the styling of a board"
  (reagent/as-element [:div.board {:style {:height 206 :width 306}}
                       [:div.cell {:style {:left 0 :top 0}}]
                       [:div.cell.wall-bottom {:style {:left 50 :top 0}}]
                       [:div.cell.wall-bottom {:style {:left 100 :top 0}}]
                       [:div.cell.wall-bottom {:style {:left 150 :top 0}}]
                       [:div.cell.wall-bottom {:style {:left 200 :top 0}}]
                       [:div.cell.last-col {:style {:left 250 :top 0}}]

                       [:div.cell.wall-right {:style {:left 0 :top 50}}]
                       [:div.cell.wall-left.wall-top {:style {:left 50 :top 50}}]
                       [:div.cell.wall-top {:style {:left 100 :top 50}}]
                       [:div.cell.wall-top {:style {:left 150 :top 50}}]
                       [:div.cell.wall-top.wall-right {:style {:left 200 :top 50}}]
                       [:div.cell.wall-left.last-col {:style {:left 250 :top 50}}]

                       [:div.cell.wall-right {:style {:left 0 :top 100}}]
                       [:div.cell.wall-left {:style {:left 50 :top 100}}]
                       [:div.cell {:style {:left 100 :top 100}}]
                       [:div.cell {:style {:left 150 :top 100}}]
                       [:div.cell.wall-right {:style {:left 200 :top 100}}]
                       [:div.cell.wall-left.last-col {:style {:left 250 :top 100}}]

                       [:div.cell.last-row.wall-right {:style {:left 0 :top 150}}]
                       [:div.cell.last-row.wall-left {:style {:left 50 :top 150}}]
                       [:div.cell.last-row {:style {:left 100 :top 150}}]
                       [:div.cell.last-row {:style {:left 150 :top 150}}]
                       [:div.cell.wall-right.last-row {:style {:left 200 :top 150}}]
                       [:div.cell.wall-left.last-row.last-col {:style {:left 250 :top 150}}]

                       [:img {:src "images/rocks.png" :style {:position "absolute" :width 50 :top 0 :left 0}}]
                       [:img.door-bottom {:src "images/door-h.png" :style {:top 0 :left 50 :position "absolute"}} ]
                       [:img.stairs {:src "images/stairs.png" :style {:top 50 :left 100 :position "absolute"}}]
                       [:img.door-right {:src "images/door-v.png" :style {:position "absolute" :top 50 :left 0}}]

                       [:img.monster {:src "images/goblin.png" :style {:position "absolute" :top 0 :left 200}}]
                       [:img.monster {:src "images/orc.png" :style {:position "absolute" :top 0 :left 250}}]
                       [:img.monster {:src "images/fimir.png" :style {:position "absolute" :top 50 :left 200}}]
                       [:img.monster {:src "images/skeleton.png" :style {:position "absolute" :top 50 :left 250}}]
                       [:img.monster {:src "images/mummy.png" :style {:position "absolute" :top 100 :left 200}}]
                       [:img.monster {:src "images/zombie.png" :style {:position "absolute" :top 100 :left 250}}]
                       [:img.monster {:src "images/warrior.png" :style {:position "absolute" :top 150 :left 200}}]
                       [:img.monster {:src "images/gargoyle.png" :style {:position "absolute" :top 147 :left 237 :width 65}}]

                       [hq/reveal-button [1 2]]
                       [hq/search-button [1 3]]
]))


(defcard action-buttons
  "Reveal (for unseen cells) and search (for traps and treasures)."
  (reagent/as-element [:div.board {:style {:height 56}}
                       [:div.cell.last-row {:style {:left 0 :top 0}}]
                       [:div.cell.last-row {:style {:left 50 :top 0}}]
                       [hq/reveal-button [0 0]]
                       [hq/search-button [1 0]]
                       [:img.door-right {:src "images/door-v.png" :style {:position "absolute" :top 0 :left 0}}]]))

(def board [{:cols 5 :rows 3}
               [{:rock true} {:seen true :doors [:bottom] :walls [:bottom]} {:walls [:bottom] :seen true} {:secret-door [:bottom] :seen true :monster :gargoyle} {:seen true}
                {:walls [:right] :seen false} {:walls [:left :top]} {:walls [:top]} {} {}
                {:walls [:right]} {:walls [:left]} {} {} {}]])

#_(hq/neighbours 0 board)

(defcard-rg generated-board
  "Magic"
  [hq/board-component (reagent/atom board)]
)

(defcard-rg determine-reveals
  "Reveals are visible"
  (fn [board _] [hq/board-component board])
  (reagent/atom board))

(def reveal-board [{:cols 6 :rows 4}
                  [{} {:doors [:bottom] :reveal true :walls [:bottom]} {:walls [:bottom]} {:secret-door [:bottom] :walls [:bottom] :monster :warrior :player :dwarf :seen true} {} {:rock true}
                      {:walls [:right] :rock true :seen false} {:walls [:left :top] :doors [:top] :seen false} {:walls [:top] :seen false} {:walls [:right :top] :doors [:right]} {:walls [:left] :doors [:left]} {:rock true}
                      {:walls [:right]} {:walls [:left :bottom] :seen false} {:seen false :walls [:bottom]} {:walls [:right :bottom]} {:walls [:left]} {}
                   {:seen true :player :dwarf :doors [:bottom]} {:walls [:top]} {:walls [:top] :doors [:top]} {:walls [:top]} {} {}]])

(hq/filtered-cells reveal-board #(:player %))

(defcard-rg revealing
  "# Revealing

When clicking a reveal-button, cells in the room or hallway will be marked as `seen`. Doors, walls and rocks are blocking.
"
  (fn [board _] [:div
                 [hq/board-component board]
                 [:br]
                 [:button {:on-click #(reset! board reveal-board)} "Reset"]])
  (reagent/atom reveal-board))

(def counter-init 0)

(defcard-rg counter
  "Some introduction about our counter"
  (fn [counter _]
    (let [{:keys [value reset]} @counter]
      [:div
       [:p value]
       [:button {:on-click #(swap! counter update-in [:value] inc)} "Click me!"]
       [:button {:on-click #(reset counter)} "Reset!"]]))
  (reagent/atom {:value counter-init :reset #(swap! % assoc-in [:value] counter-init)}))

#_(defcard-rg isolated-simple-board
  (fn [data _] [hq/board @data])
  simple-board
  {:inspect-data true})
