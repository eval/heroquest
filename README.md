# heroquest


End-result:

<http://eval.gitlab.io/heroquest/>

Devcards:

<http://eval.gitlab.io/heroquest/cards.html>

standalone:

```
$ lein figwheel devcards dev
# visit http://localhost:3449/cards.html and http://localhost:3449/
```

1). eval in emacs
    (setq cider-cljs-lein-repl "(do (use 'figwheel-sidecar.repl-api) (start-figwheel!) (cljs-repl))")
1). start cider
    M-x cider-jack-in-clojurescript
1). visit http://localhost:3449/cards.html
1). to visit http://localhost:3449/ start an extra autobuild:
    cljs.user> (start-autobuild :devcards)


(add-hook 'cider-repl-mode-hook #'eldoc-mode)
(setq cider-repl-tab-command #'indent-for-tab-command)

## Overview

FIXME: Write a paragraph about the library/project and highlight its goals.

## Setup

To get an interactive development environment run:

    lein figwheel

and open your browser at [localhost:3449](http://localhost:3449/).
This will auto compile and send all changes to the browser without the
need to reload. After the compilation process is complete, you will
get a Browser Connected REPL. An easy way to try it is:

    (js/alert "Am I connected?")

and you should see an alert in the browser window.

To clean all compiled files:

    lein clean

To create a production build run:

    lein do clean, cljsbuild once min

And open your browser in `resources/public/index.html`. You will not
get live reloading, nor a REPL. 

## License

Copyright © 2014 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.

